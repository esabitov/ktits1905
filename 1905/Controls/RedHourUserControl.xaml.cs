﻿using _1905.Interfaces;
using _1905.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _1905.Controls
{
    /// <summary>
    /// Логика взаимодействия для RedHourUserControl.xaml
    /// </summary>
    public partial class RedHourUserControl : UserControl ,IHourUserControl
    {

        private Page _CalendarPage { get; set; }
        public RedHourUserControl(Page calendarPage  )
        {
            InitializeComponent();  
            _CalendarPage = calendarPage;   
        }

        private void DoSomethingButton_Click(object sender, RoutedEventArgs e)
        {
            _CalendarPage.NavigationService.Navigate(new HourContentPage());
         
        }

        public void DoSomething()
        {
            MessageBox.Show("На меня нажали");
        }
    }
}
