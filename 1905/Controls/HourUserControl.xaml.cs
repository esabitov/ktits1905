﻿using _1905.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _1905.Controls
{
    /// <summary>
    /// Логика взаимодействия для HourUserControl.xaml
    /// </summary>
    public partial class HourUserControl : UserControl, IHourUserControl
    {
        public HourUserControl()
        {
         
            InitializeComponent();
        }
        public void DoSomething()
        {
            MessageBox.Show("На меня нажали");
        }
    }
}
