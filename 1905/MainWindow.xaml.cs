﻿using _1905.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _1905
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            MainFrame.Navigate(new AuthPage());
            this.Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.MainFrame.NavigationService.Navigating += NavigationService_Navigating;
            this.MainFrame.NavigationService.Navigated += NavigationService_Navigated;
            this.MainFrame.NavigationService.LoadCompleted += NavigationService_LoadCompleted;
        }

        private void NavigationService_Navigating(object sender, NavigatingCancelEventArgs e)
        {
       //  MainFrame.NavigationService.StopLoading();     
        }

        private void NavigationService_LoadCompleted(object sender, NavigationEventArgs e)
        {

        }

        private void NavigationService_Navigated(object sender, NavigationEventArgs e)
        {

        }
    }
}
