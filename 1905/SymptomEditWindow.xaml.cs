﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using _1905.Models;
using _1905.ViewModels;

namespace _1905
{
    /// <summary>
    /// Логика взаимодействия для SymptomEditWindow.xaml
    /// </summary>
    public partial class SymptomEditWindow : Window
    {
        public SymptomItem Symptom { get; }
    public string SymptomTitle { get; set; }
    public int SymptomPriority { get; set; }
        public SymptomEditWindow(BindingExampleViewModel vm)
        {
            Symptom = vm.SelectedSymptomItem;
            SymptomPriority = Symptom.Priority;
            SymptomTitle = Symptom.Title;
            InitializeComponent();
            DataContext = this;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Symptom.Title = SymptomTitle;
            Symptom.Priority = SymptomPriority;
            
        }
    }
}
