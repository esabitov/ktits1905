﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace _1905.Models
{
    public class SymptomResult
    {
        [JsonPropertyName("data")]
        public List<SymptomItem> Data { get; set; }
    }
}
