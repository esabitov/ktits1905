﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace _1905.Models
{
    public class SymptomItem: INotifyPropertyChanged
    {
        private int _id;
        private string _title;
        private int _priority;

        [JsonPropertyName("id")]
        public int Id
        {
            get => _id;
            set => _id = value;
        }

        [JsonPropertyName("title")]
        public string Title
        {
            get => _title;
            set {  _title = value; OnPropertyChanged(); }
        }

        [JsonPropertyName("priority")]
        public int Priority
        {
            get => _priority;
            set => _priority = value;
        }

        public string TextPriority
        {
            get
            {

                if (Priority > 5)
                {
                    return "Часто";
                }
                else
                {
                    return "Редко";
                }
            }
        }

        public event PropertyChangedEventHandler? PropertyChanged;


        public void OnPropertyChanged([CallerMemberName] string propertyname = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
            }
        }
    }
}
