﻿using _1905.Controls;
using _1905.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _1905.Pages
{
    /// <summary>
    /// Логика взаимодействия для CalendarPage.xaml
    /// </summary>
    public partial class CalendarPage : Page
    {
        public CalendarPage()
        {
            InitializeComponent();
            this.Loaded += CalendarPage_Loaded;   
        }

        private void CalendarPage_Loaded(object sender, RoutedEventArgs e)
        {
            var hours = new List<UserControl>();
            for (var i = 0; i < 24; i++)
            {
                if (i % 5 == 0)
                {
                    hours.Add(new RedHourUserControl(this));
                }
                else
                {
                    hours.Add(new HourUserControl());
                }
            }
            DayListView.ItemsSource = hours;

            DayListView.SelectionChanged += DayListView_SelectionChanged;
        }

        private void DayListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ((sender as ListView).SelectedItem as IHourUserControl).DoSomething();
        }
    }
}
