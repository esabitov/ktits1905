﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _1905.Pages
{
    /// <summary>
    /// Логика взаимодействия для HourContentPage.xaml
    /// </summary>
    public partial class HourContentPage : Page
    {
        public HourContentPage()
        {
            InitializeComponent();

            this.Loaded += HourContentPage_Loaded;
            
        }

        private void HourContentPage_Loaded(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigating += NavigationService_Navigating1;
        }

        private void NavigationService_Navigating1(object sender, NavigatingCancelEventArgs e)
        {
            this.NavigationService.StopLoading();
         
        }

       
    }
}
