﻿using _1905.Models;
using _1905.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _1905.Pages
{
    /// <summary>
    /// Логика взаимодействия для BindingExamplePage.xaml
    /// </summary>
    public partial class BindingExamplePage : Page
    {
        public BindingExampleViewModel ViewModel { get; set; }
        public BindingExamplePage()
        {
            ViewModel = new BindingExampleViewModel();  
            InitializeComponent();

            GetDataFromServer();
        }
        public void GetDataFromServer()
        {
            var wc = new WebClient();

            var result = wc.DownloadString("https://wsa2021.mad.hakta.pro/api/symptom_list");
            var data =
                    JsonSerializer.Deserialize<SymptomResult>(result);
            //  SymptomList = data.Data ;   8
            foreach (var item in data.Data)
            {
                ViewModel.SymptomItems.Add(item);
            }
              

        }

        private void ChangeDataInsideButton_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.SymptomItems[1].Title = "Lallaala";
        }

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        
            
                var w = new SymptomEditWindow(ViewModel);
                w.Show();
         
        }
    }
}
