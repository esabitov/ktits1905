﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.Json;
using _1905.Models;
using System.Collections.ObjectModel;

namespace _1905.Pages
{
    /// <summary>
    /// Логика взаимодействия для SymptomListPage.xaml
    /// </summary>
    public partial class SymptomListPage : Page
    {
        public SymptomListPage()
        {
            GetDataFromServer();
            InitializeComponent();
            this.DataContext = this;
          
        }

        public ObservableCollection<SymptomItem> SymptomList { get; set; }
       
        public void GetDataFromServer()
        {
            var wc = new WebClient();

            var result = wc.DownloadString("https://wsa2021.mad.hakta.pro/api/symptom_list");
            var data = 
                    JsonSerializer.Deserialize<SymptomResult>(result);
          //  SymptomList = data.Data ;
            SymptomList = new ObservableCollection<SymptomItem>(data.Data);

        }

        private void AddSymptomButton_Click(object sender, RoutedEventArgs e)
        {
            SymptomList.Add(
                new SymptomItem() { Id = 99, Title = "New Item" }
            );

        }
    }
}
