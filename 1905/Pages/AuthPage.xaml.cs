﻿using _1905.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.Json;
namespace _1905.Pages
{
    /// <summary>
    /// Логика взаимодействия для AuthPage.xaml
    /// </summary>
    public partial class AuthPage : Page
    {
        public AuthPage()
        {
            InitializeComponent();
            MessageBox.Show(Properties.Settings.Default["UserToken"] as string);
        }
        public SymptomItem ViewModel { get; set; }

        private void SignInButton_Click(object sender, RoutedEventArgs e)
        {
            var requestData = new SignInRequest()
            { Login = UserLogin.Text, Password = UserPassword.Text };

             
            var url = "https://wsa2021.mad.hakta.pro/api/signin/";
            var wc = new WebClient();
            var jsonData = JsonSerializer.Serialize(requestData);
            var data =  wc.UploadString(url, jsonData);
            var authResponse = JsonSerializer.Deserialize<SignInResponse>(data);
            var token =    authResponse.data.token;

            //
            Properties.Settings.Default.UserToken = token;
            Properties.Settings.Default.Save();


        }
    }
}
