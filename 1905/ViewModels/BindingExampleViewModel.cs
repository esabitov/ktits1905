﻿using _1905.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace _1905.ViewModels
{
    public class BindingExampleViewModel : INotifyPropertyChanged
    {

        private ObservableCollection<SymptomItem> _symptomItems = new ObservableCollection<SymptomItem>();
        private SymptomItem selectedSymptomItem;

        public ObservableCollection<SymptomItem> SymptomItems { get => _symptomItems; set => _symptomItems = value; }

        public int FontSize { get; set; } = 16;
        public int SymptomsCount { get { return SymptomItems.Count(); } }


        public SymptomItem SelectedSymptomItem { get => selectedSymptomItem;
            set { selectedSymptomItem = value; OnPropertyChanged(); }
        }
















        public event PropertyChangedEventHandler? PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string propertyname = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
            }
        }


    }
}
